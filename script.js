let menu = document.querySelector('#menu-bars');
let navbar = document.querySelector('.navbar');

menu.onclick = () => {
    menu.classList.toggle('fa-times');
    navbar.classList.toggle('active');
}

let section = document.querySelectorAll('section');
let navLinks = document.querySelectorAll('header .navbar a');

window.onscroll = () => {
    menu.classList.remove('fa-times');
    navbar.classList.remove('active');

    // wishList

    document.querySelector('.shopping-cart').classList.remove('active');

    // to change navbar links active according selected section
    
    section.forEach(sec => {
        let top = window.scrollY;
        let height = sec.offsetHeight;
        let offset = sec.offsetTop - 150;
        let id = sec.getAttribute('id');

        if(top >= offset && top < offset + height){

            navLinks.forEach(links => {
                links.classList.remove('active');
                document.querySelector('header .navbar a[href*='+id+']').classList.add('active');
            });
        }
    });
}

document.querySelector('#search-icon').onclick = () =>{
    document.querySelector('#search-form').classList.toggle('active');
}

document.querySelector("#close").onclick = () => {
    document.querySelector("#search-form").classList.remove('active');
}

document.querySelector('#cart-btn').onclick = () =>{
    document.querySelector('.shopping-cart').classList.toggle('active');
}




// adding and removing items to wishlist

// adding to cart

var addCartButtons = document.getElementsByClassName('shop-item-button');

for(var i = 0; i < addCartButtons.length; i++){

    var button = addCartButtons[i];

    button.addEventListener('click', addToCartClicked);

}

// remove button

var removeCartItemsButtons = document.getElementsByClassName('fa-trash');

console.log(removeCartItemsButtons);

for(var i = 0; i < removeCartItemsButtons.length; i++){

    var button = removeCartItemsButtons[i];

    button.addEventListener('click', removeCartItem)
}

// input value change button

var quantityInputs = document.getElementsByClassName('cart-quantity-input');

for(var i = 0 ; i < quantityInputs.length; i++){

    var input = quantityInputs[i];

    input.addEventListener('change', quantityChanged);
}

// when button click and addeventlistener activated , it will send EVENT as object parameter

// addToCart clicked function

function addToCartClicked(event){

    var button = event.target;

    var shopItem = button.parentElement.parentElement;

    var title = shopItem.getElementsByClassName('shop-item-title')[0].innerHTML;
    var price = shopItem.getElementsByClassName('price')[0].innerHTML;
    var imageSrc = shopItem.getElementsByClassName('shop-item-img')[0].src;

    console.log(title, price, imageSrc);

    addItemToCart(title, price, imageSrc);

    updateCartTotal(); //it is necessary otherwise total won't update when you add items

}

function addItemToCart(title, price, imageSrc){

    //creating a element in-form of div and adding style to it using classlist.add

    var cartRow = document.createElement('div');
    cartRow.classList.add('cart-row');

    var cartItems = document.getElementsByClassName('cart-items')[0]
    var cartItemsNames = cartItems.getElementsByClassName('cart-item-title');

    for(var i = 0; i < cartItemsNames.length; i++){
        if(cartItemsNames[i].innerHTML == title){
            alert('this item has been added already, check the cart.');
            return;
        }
    }

    var cartRowContents = `
        <div class="cart-item cart-column">
            <img src="${imageSrc}" width="100" height="100" class="cart-item-img" alt="thumb">
            <span class="cart-item-title">${title}</span>
        </div>

        <span class="cart-price cart-column">${price}</span>

        <div class="cart-quantity cart-column">
            <input type="number" value="1" class="cart-quantity-input">
            <i class="fas fa-trash"></i>
        </div>`;
    
    cartRow.innerHTML = cartRowContents;
    
    cartItems.append(cartRow);

    cartRow.getElementsByClassName('fa-trash')[0].addEventListener('click', removeCartItem);
    cartRow.getElementsByClassName('cart-quantity-input')[0].addEventListener('change', quantityChanged);

}

// removeCartItem function

function removeCartItem(event){
  var buttonClicked = event.target;
  buttonClicked.parentElement.parentElement.remove();

  //to update total of the cart
  updateCartTotal();
}

// quantityChanged function

function quantityChanged(event){
    var input = event.target;

    if(isNaN(input.value) || input.value <= 0){
        input.value = 1;
    }

    updateCartTotal();
}

// updating total cart

function updateCartTotal(){

  //assigning main parent div to access the elements

  var cartItemContainer = document.getElementsByClassName('cart-items')[0]; //[0] access first element of cart-item div

  var cartRows = cartItemContainer.getElementsByClassName('cart-row'); // dont use document. it will access other cart-row classes from other divs

  var total = 0;

  for(var i = 0; i < cartRows.length; i++){
      var cartRow = cartRows[i]; // to access individuals row

      //now accesing elements like price and quantity from cart-row 
      var priceElement = cartRow.getElementsByClassName('cart-price')[0];

      var quantityElement = cartRow.getElementsByClassName('cart-quantity-input')[0];

      // here access values by using innerElement or other properties
      var price = parseFloat(priceElement.innerHTML.replace('Rs:', '')); //accesing, replacing Rs: with empty string and converting string to number.

      var quantity = quantityElement.value; //input has values not innerHtml

      // maths

      total = total + (price * quantity);
  }

  // rounding value of total

  total = Math.round(total * 100) / 100;

  // returning total to calling function

  document.getElementsByClassName('cart-total-price')[0].innerHTML ='Rs:' + total;
}



//purchase button

document.getElementsByClassName('btn-purchase')[0].addEventListener('click', purchaseClicked);

function purchaseClicked(){
    

    // remove data from cart after purchase and updating total

    var cartItems = document.getElementsByClassName('cart-items')[0];

    
    if(cartItems.hasChildNodes()){
        alert("Thank you for your purchase!");
    }

    while(cartItems.hasChildNodes()){
        cartItems.removeChild(cartItems.firstChild);
    }


    //updating total
    updateCartTotal();
}

